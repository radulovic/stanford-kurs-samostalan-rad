//
//  GraphViewController.swift
//  Prvi zadatak
//
//  Created by Milan on 9/7/17.
//  Copyright © 2017 Milan. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var graphView: GraphModel! {
        
        didSet {
            
            initGestures()
            
            updateUI()
        }
    }
    
    private func initPinch() {
        let pinchRecognizer = UIPinchGestureRecognizer(target: graphView, action: #selector(GraphModel.pinchGesture(byRecognizing:)))
        graphView.addGestureRecognizer(pinchRecognizer)
    }
    
    private func initTap() {
        let tapRecognizer = UITapGestureRecognizer(target: graphView, action: #selector(GraphModel.tapGesture(byRecognizing:)))
        tapRecognizer.numberOfTapsRequired = 1
        graphView.addGestureRecognizer(tapRecognizer)
    }
    
    private func initPan() {
        let panRecognizer = UIPanGestureRecognizer(target: graphView, action: #selector(GraphModel.panGesture(byRecognizing:)))
        graphView.addGestureRecognizer(panRecognizer)
    }
    
    private func initGestures() {
        initPinch()
        initTap()
        initPan()
    }
    
    //MARK: - Update UI
    func updateUI() {
        graphView?.XvsY = XvsY
    }
    
    var XvsY: ((Double) -> Double)? {
        didSet {
            updateUI()
        }
    }

}
