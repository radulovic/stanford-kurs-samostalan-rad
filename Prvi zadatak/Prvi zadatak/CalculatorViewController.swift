
import UIKit
import Foundation

class CalculatorViewController: UIViewController, UISplitViewControllerDelegate {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.splitViewController?.delegate = self
    }
    
    func splitViewController(_ splitViewController: UISplitViewController,
                             collapseSecondary secondaryViewController: UIViewController,
                             onto primaryViewController: UIViewController
        )-> Bool {
        if primaryViewController.contents == self {
                return true
        }
        return false
    }

    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        brain.addUnaryOperation(named: "✅") { [weak weakSelf = self] in
            weakSelf?.display.textColor = UIColor.green
            return sqrt($0)
        }
    }
    
    //MARK: - Variables
    private var brain = CalculatorBrain()
    private var userIsTyping = false
    private var arrayOfUsedOperands = [Double]()
    
    //MARK: - Outlets
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var operationDescription: UILabel!
    @IBOutlet weak var pboLabel: UILabel!
    @IBOutlet weak var pboIndicator: UIActivityIndicatorView!
    @IBOutlet weak var currentM: UILabel!
    
    //MARK: - Display
    var displayValue: String {
        get {
            return display.text ?? ""
        }
        set {
            
            if newValue.characters.count > 6 {
                
                let formatter = NumberFormatter()
                formatter.roundingMode = NumberFormatter.RoundingMode.halfUp
                formatter.maximumFractionDigits = 6
                let str = formatter.number(from: newValue)
                
                display.text = formatter.string(from: str!)
            } else {
                display.text = String(newValue)
            }
        }
    }
    
    func setIndicators(_ set: Bool) {
        if set {
            pboLabel.isHidden = false
            pboIndicator.startAnimating()
        } else {
            pboLabel.isHidden = true
            pboIndicator.stopAnimating()
        }
    }
    
    private func resetView() {
        displayValue = "0.0"
        brain.description = ""
        operationDescription.text = brain.description
    }
    
    //MARK: - touchDigit
    @IBAction func touchDigit(_ sender: UIButton) {
        
        let digit = sender.currentTitle!
        
        if userIsTyping {
            
            let currentlyOnDispay = display.text!
            let newToCheck = currentlyOnDispay + digit
            if isValid(newToCheck) {
                display.text = newToCheck
            }
            
        } else {
            
            let newToCheck = digit
            if isValid(newToCheck) {
                display.text = newToCheck
                userIsTyping = true
            }
        }
    }
    
    //MARK: - Performing operations
    @IBAction func performOperation(_ sender: UIButton) {
        
        arrayOfUsedOperands.append(Double(displayValue)!)
        
        if userIsTyping {
            brain.setOperand(Double(displayValue)!)
            userIsTyping = false
        }
        
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(mathematicalSymbol)
            let bool = brain.resultIsPending
            setIndicators(bool)
        }
        
        getResult()
        
        
        operationDescription.text = brain.description
    }
    
    func getResult() {
        
        if brain.evaluate().result != nil {
            displayValue = String(brain.evaluate().result!)
            operationDescription.text = brain.evaluate().description
            let bool = brain.evaluate().isPending
            setIndicators(bool)
        }
    }
    
    @IBAction func backspace() {
        
        var char = display.text!
        if char != "" {
            char.remove(at: char.index(before: char.endIndex))
            if char == "" {
                displayValue = "0"
                userIsTyping = false
            } else {
                displayValue = char
            }
        }
    }
    
    //MARK: - Validating before displaying
    func isValid(_ toCheck: String) -> Bool {
        
        var valid = true
        
        if toCheck.hasPrefix("00") || toCheck.hasPrefix(".")  || dotCounter(toCheck) {
            valid = false
        }
        
        return valid
    }
    
    private func dotCounter(_ toCheck: String) -> Bool {
        var DotCounter = 0
        
        for c:Character in toCheck.characters {
            if c == "." {
                DotCounter += 1
            }
        }
        if DotCounter > 1 {
            return true
        } else {
            return false
        }
    }
    
    //MARK: - Undo button
    @IBAction func undo() {
        
        if userIsTyping {
            backspace()
        } else {
            
            if !arrayOfUsedOperands.isEmpty {
                brain.undo(arrayOfUsedOperands)
                arrayOfUsedOperands.remove(at: arrayOfUsedOperands.count - 1)
            } else {
                brain.setOperand(0)
                resetView()
            }
            getResult()
        }
    }
    
    //MARK: - Clear button
    @IBAction func clear(_ sender: UIButton) {
        
        brain.reset()
        resetView()
        userIsTyping = false
        variables["M"] = 0
        brain.setM(0)
        currentM.text = "M: " + String(brain.getM())
    }
    
    //MARK: - Rand generator
    @IBAction func generateRandom(_ sender: UIButton) {
        
        let random = drand48()
        displayValue = String(random)
        brain.setOperand(random)
        brain.description = String(random)
    }
    
    //MARK: - M buttons
    var variables: Dictionary<String, Double> = [
        "M" : 0
    ]
    
    //MARK: M's setter and getter
    @IBAction func setM(_ sender: UIButton) {
        
        brain.setM(Double(displayValue)!)
        currentM.text = "M: " + String(displayValue)!
        userIsTyping = false
       
    }
    
    @IBAction func getM(_ sender: UIButton) {
        
        displayValue = String(brain.getM())
        brain.setOperand(Double(displayValue)!)
    }
    
    //MARK: - Prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        var destinationViewController = segue.destination
        
        if let navigationController = destinationViewController as? UINavigationController {
            destinationViewController = navigationController.visibleViewController ?? destinationViewController
        }
        
        if let graphViewController = destinationViewController as? GraphViewController,
            let identifier = segue.identifier,
            identifier == "showGraph" {
            
            graphViewController.XvsY = brain.graphFunction
        }
    }
}

extension UIViewController {
    
    var contents: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? self
        } else {
            return self
        }
    }
}
