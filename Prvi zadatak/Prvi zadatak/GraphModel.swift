//
//  GraphModel.swift
//  Prvi zadatak
//
//  Created by Milan on 9/7/17.
//  Copyright © 2017 Milan. All rights reserved.
//

import UIKit

@IBDesignable
class GraphModel: UIView {
    
    //MARK: - Variables
    @IBInspectable
    var pointsPerUnit: CGFloat = 50 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var lineWidth: CGFloat = 2.0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var lineColor: UIColor = .black {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var functionLineColor: UIColor = .red {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var origin: CGPoint? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    private var originGetAndSet: CGPoint {
        get {
            return origin ?? CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        }
        set {
            origin = newValue
        }
    }
    
    var XvsY: ((Double) -> Double)? {
        didSet {
            setNeedsDisplay()
        }
    }
    
    //MARK: - Functions
    private func drawCoordinateSystem() {
        let axesDrawer = AxesDrawer(color: lineColor, contentScaleFactor: contentScaleFactor)
        axesDrawer.drawAxes(in: bounds, origin: originGetAndSet, pointsPerUnit: pointsPerUnit)
    }
    
    //MARK: - Drawing function line
    private func drawFunction(in rect: CGRect) {
        let path = UIBezierPath()
        path.lineWidth = lineWidth
        functionLineColor.set()
        
        var x, y: Double
        var xCG, yCG: CGFloat
        var firstPoint = true
        var discontinuity: Bool {
            return yCG > max(bounds.width, bounds.height)
        }
        
        if XvsY != nil {
            
            let range = Int(bounds.size.width * contentScaleFactor)
            
            for i in 0...range {
                
                xCG = CGFloat(i) / contentScaleFactor
                x = Double((xCG - originGetAndSet.x) / pointsPerUnit)
                y = XvsY!(x)
                
                if !y.isFinite { continue }
                
                yCG = originGetAndSet.y - CGFloat(y) * pointsPerUnit
                
                if firstPoint {
                    path.move(to: CGPoint(x: xCG, y: yCG))
                    firstPoint = false
                } else {
                    if discontinuity {
                        firstPoint = true
                    } else {
                        path.addLine(to: CGPoint(x: xCG, y: yCG))
                    }
                }
            }
        }
        
        path.stroke()
    }
    
    //MARK: - Draw
    override func draw(_ rect: CGRect) {
        drawCoordinateSystem()
        drawFunction(in: rect)
    }
    
    //MARK: - Gestures
    func pinchGesture(byRecognizing pinch: UIPinchGestureRecognizer) {
        switch pinch.state {
        case .changed, .ended:
            pointsPerUnit *= pinch.scale
            pinch.scale = 1
        default:
            break
        }
    }
    
    //MARK: - Panning
    func panGesture(byRecognizing pan: UIPanGestureRecognizer) {
        switch pan.state {
        case .changed:
            let translation = pan.translation(in: self)
            if translation != CGPoint(x: 0, y: 0) {
                originGetAndSet.x += translation.x
                originGetAndSet.y += translation  .y
                pan.setTranslation(CGPoint(x: 0, y: 0), in: self)
            }
        default:
            break
        }
    }
    
    //MARK: - Tapping
    func tapGesture(byRecognizing tap: UITapGestureRecognizer) {
        switch tap.state {
        case .ended:
            originGetAndSet = tap.location(in: self)
        default:
            break
        }
    }
    
}
