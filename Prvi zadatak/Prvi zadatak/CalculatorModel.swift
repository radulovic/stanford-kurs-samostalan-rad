
import Foundation

struct CalculatorBrain {
    
    //MARK: - Private variables
    private var accumulator: Double?
    private var pendingBinaryOperation: PendingBinaryOperation?
    private var M: Dictionary<String, Double> = [
        "M" : 0.0
    ]
    
    //MARK: - Public variables
    var resultIsPending = false
    var description = ""
    var graphFunction: ((Double) -> Double)?
    
    //MARK: - Operations
    private enum Operation {
        case constant(Double)
        case unary((Double) -> Double)
        case binary((Double, Double) -> Double)
        case equals
    }
    
    //MARK: List of operations
    private var operations: Dictionary<String, Operation> = [
        "+"     : Operation.binary({ $0 + $1 }),   //summation
        "−"     : Operation.binary({ $0 - $1 }),   //subtraction
        "×"     : Operation.binary({ $0 * $1 }),   //multiplication
        "÷"     : Operation.binary({ $0 / $1 }),   //division
        "sin"   : Operation.unary(sin),            //sine
        "cos"   : Operation.unary(cos),            //cosine
        "tan"   : Operation.unary(tan),            //tangents
        "ctg"   : Operation.unary({ 1 / tan($0) }),//cotangent
        "√"     : Operation.unary(sqrt),           //square root
        "π"     : Operation.constant(Double.pi),   //Pi
        "e"     : Operation.constant(M_E),         //e
        "x²"    : Operation.unary({ $0 * $0  }),   //square
        "x³"    : Operation.unary({ $0 * $0 * $0 }),//cub
        "±"     : Operation.unary({ -$0 }),        //change sign
        "%"     : Operation.unary({ $0 / 100 }),   //percents
        "="     : Operation.equals,                //equals
    ]
    
    //MARK: - Public API
    mutating func reset() {
        
        accumulator = nil
        pendingBinaryOperation = nil
        description = ""
    }
    
    mutating func setOperand(_ operand: Double) {
        accumulator = operand
        
    }
    
    mutating func setM(_ value: Double) {
        M["M"] = value
    }
    
    func getM() -> Double {
        return M["M"]!
    }
    var result: Double? {
        get {
            return accumulator
        }
    }
    
    mutating func performOperation(_ simbol: String) {
        
        if let operation = operations[simbol] {
            
            switch operation {
            case .constant(let value):
                setOperand(value) // same as: accumulator = value
                description = simbol
                
            case .unary(let function):
                if accumulator != nil {
                    description = simbol + "(" + String(accumulator!) + ")"
                    graphFunction = function
                    accumulator = function(accumulator!)
                }
                
            case .binary(let function):
                if accumulator != nil {
                    pendingBinaryOperation = PendingBinaryOperation(function: function, firstOperand: accumulator!)
                    resultIsPending = true
                    description = String(accumulator!) + simbol + "..."
                    accumulator = nil
                }
                
            case .equals:
                performPendingBinaryOperation()
            }
        }
    }
    
    mutating func undo(_ operands: [Double]) {
        
        if !operands.isEmpty {
            let prevOperand = operands[operands.count - 1]
            setOperand(prevOperand)
            description = String(prevOperand)
        }
    }
    
    mutating func addUnaryOperation(named symbol: String, _ operation: @escaping (Double) -> Double) {
        
        operations[symbol] = Operation.unary(operation)
    }
    
    //MARK: - Evaluate
    func evaluate(using variables: Dictionary<String,Double>? = nil) -> (result: Double?, isPending: Bool, description: String) {
        
        let resultOfEvaluate = result
        let isPending = resultIsPending
        let descriptionOfEvaluate = description
        
        return (resultOfEvaluate,isPending, descriptionOfEvaluate)
    }
    
    //MARK: - Private methods
    private func remove3Dots(_ word: String) -> String {
        let endIndex = word.index(word.endIndex, offsetBy: -3)
        let truncated = word.substring(to: endIndex)
        
        return truncated
    }
    
    private mutating func performPendingBinaryOperation() {
        
        if pendingBinaryOperation != nil && accumulator != nil {
            description = remove3Dots(description)
            description.append(String(accumulator!) + "=")
            accumulator = pendingBinaryOperation!.perform(with: accumulator!)
            pendingBinaryOperation = nil
            resultIsPending = false
        }
    }
    
    private struct PendingBinaryOperation {
        
        let function: (Double, Double) -> Double
        let firstOperand: Double
        
        func perform(with secondOperand: Double) -> Double {
            return function(firstOperand, secondOperand)
        }
    }
}
